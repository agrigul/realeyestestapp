﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Web.Helpers;
using System.Web.Mvc;
using DomainModel.Entities;
using DomainModel.Repositories;
using DomainModel.Services;
using Implementation.DAL;
using Implementation.DAL.Repositories;
using Implementation.DAL.Services;

namespace RealEyesTestApp.Controllers
{
    public class HomeController : Controller
    {
        // NOTE: The first run of homepage will be longer, because the database will be droped and recreated
        // Also the rates have to be downloaded and saved in DB. In real apps the separate serve perfoms this async,
        // but our app can't work without this data, so we have to wait.
        // On the next day only latests(new) rates will be added to database.
        public ActionResult Index()
        {
            Url url = new Url("http://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml");
            IParseXmlCurrencyRateService ecbXmlParsingService = new ParseEcbXmlCurrencyRatesService();
            IEcbRemoteRepository ecbRemoteRepository = new EcbRemoteRepository(ecbXmlParsingService, url);
            IList<CurrencyRate> listOfRates = ecbRemoteRepository.GetAllRates();


            using (var dbContext = new CurrencyDbContext())
            {
                ICurrencyRatesRepository currencyRatesRepository = new CurrencyRatesRepository(dbContext);
                currencyRatesRepository.SaveOnlyNewRates(listOfRates);
            }

            return View();
        }


        [HttpGet]
        public ActionResult GetLatestRates()
        {
            IList<CurrencyRate> currencyRatesList;

            using (var dbContext = new CurrencyDbContext())
            {
                ICurrencyRatesRepository currencyRatesRepository = new CurrencyRatesRepository(dbContext);
                currencyRatesList = currencyRatesRepository.GetLatestRates();
            }


            return Json(currencyRatesList, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetRatesHistory(string currencyType)
        {
            IList<CurrencyRate> currencyRatesList;

            using (var dbContext = new CurrencyDbContext())
            {
                ICurrencyRatesRepository currencyRatesRepository = new CurrencyRatesRepository(dbContext);
                currencyRatesList = currencyRatesRepository.GetRatesHistory(currencyType);
            }


            return Json(currencyRatesList, JsonRequestBehavior.AllowGet);
        }



        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}