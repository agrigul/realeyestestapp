﻿var Controls = Controls || {}


Controls.RatesDropdownList = function (exchangeViewModel, hisotryRatesDataSource, viewModelRateDirection) {
    
    var updateViewMode = function (propertyName, value) {
        exchangeViewModel.set(propertyName, value);
        exchangeViewModel.change();
    }

    var historyRatesDs = hisotryRatesDataSource;
    
    return {
        dataTextField: "RateType",
        dataValueField: "Rate",
        dataSource: exchangeViewModel.dataSource,

        change: function () {

            updateViewMode(viewModelRateDirection, this.value()); 

            if (historyRatesDs) {
                var rateType = this.dataItem(this.select()).RateType;
                historyRatesDs.read({ currencyType: rateType });
            }
        },
        dataBound: function () {
            updateViewMode(viewModelRateDirection, this.value());
        }
    }
};