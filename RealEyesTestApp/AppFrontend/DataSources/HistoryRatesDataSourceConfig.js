﻿var DataSources = DataSources || {}


DataSources.HistoryRatesDataSourceConfig = function (graph, historyRateSeries, color, index) {

    var getRatesFromDataSource = function (data) {

        var rateName = data[0].RateType;
        var newRates = [];
        for (var i = 0; i < data.length; i++) {
            var date = Math.round(window.kendo.parseDate(data[i].RateDate).getTime()) / 1000;
            var rate = { x: date, y: data[i].Rate };
            newRates.push(rate);
        }

        return { data: newRates, rateName: rateName };
    }

    return {
        type: "aspnetmvc-ajax",
        transport: {
            read: {
                type: "GET",
                url: window.location.origin + "/Home/GetRatesHistory",
                dataType: "json"

            }
        },

        change: function () {
            var newRatesObject = getRatesFromDataSource(this.view());
            historyRateSeries[index] = {
                data: newRatesObject.data,
                color: color,
                name: newRatesObject.rateName
            };
            graph.update();

        }
    }
};