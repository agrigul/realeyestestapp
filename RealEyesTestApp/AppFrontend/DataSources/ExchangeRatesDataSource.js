﻿var DataSources = DataSources || {}


DataSources.ExchangeRatesDataSource = function () {

    return new window.kendo.data.DataSource({
        type: "aspnetmvc-ajax",
        transport: {
            read: {
                type: "GET",
                url: window.location.origin + "/Home/GetLatestRates",
                dataType: "json"
            }
        }
    });
}