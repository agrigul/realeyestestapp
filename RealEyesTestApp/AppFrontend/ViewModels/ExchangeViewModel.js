﻿var ViewModels = ViewModels || {}

ViewModels.ExhangeViewModel = (function (exchangeRatesDataSource) {

    var exchangeViewModel = window.kendo.observable({
        fromValue: 0,
        toValue: 0,
        fromRate: 0,
        toRate: 0,
        dataSource: exchangeRatesDataSource,
        change: function (e) {
            if (this.fromRate !== 0) {
                var result = (this.fromValue * this.toRate) / this.fromRate;
                this.set("toValue", Number(result).toFixed(2));
            }
        }
    });

    return exchangeViewModel;
})