using System.Data.Entity;

namespace Implementation.DAL
{
    public class CurrencyDbInitializer : DropCreateDatabaseIfModelChanges<CurrencyDbContext>
    {
        protected override void Seed(CurrencyDbContext context)
        {
        }
    }
}