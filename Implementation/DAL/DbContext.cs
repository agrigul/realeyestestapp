﻿using System.Data.Entity;
using Implementation.DAL.Mappings;

namespace Implementation.DAL
{
    /// <summary>
    /// Context for currency rates history database
    /// </summary>
    public class CurrencyDbContext : DbContext
    {
        public CurrencyDbContext()
            : base("CurrencyDBConnection")
        {
        }


        public CurrencyDbContext(IDatabaseInitializer<CurrencyDbContext> initializer)
            : base("CurrencyDBConnection")
        {
            Database.SetInitializer(initializer);
        }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new MapCurrencyRate());
            base.OnModelCreating(modelBuilder);
        }

    }
}
