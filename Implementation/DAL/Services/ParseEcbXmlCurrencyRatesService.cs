﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Security.Policy;
using System.Xml;
using DomainModel.Entities;
using DomainModel.Services;
using DomainModel.Utilities;

namespace Implementation.DAL.Services
{
    public class ParseEcbXmlCurrencyRatesService : IParseXmlCurrencyRateService
    {
        private const string CubeNodeName = "Cube";
        private const string RateAttributeName = "rate";
        private const string TypeAttributeName = "currency";
        private const string CubeTimeAttribute = "time";


        public IList<CurrencyRate> GetListOfRates(XmlTextReader reader)
        {
            Assert.IsNotNull(reader, "reader", typeof(XmlTextReader));

            IList<CurrencyRate> currencyRates = new List<CurrencyRate>();

            DateTime cubeDateTimeAttributeValue = DateTime.MinValue;
            while (reader.Read())
            {
                var newCurrencyRateEntity = ParseCubeNode(reader, ref cubeDateTimeAttributeValue);
                if (newCurrencyRateEntity != null)
                    currencyRates.Add(newCurrencyRateEntity);
            }

            return currencyRates;
        }

        private CurrencyRate ParseCubeNode(XmlTextReader reader, ref DateTime cubeDateTimeAttributeValue)
        {
            if (reader.NodeType == XmlNodeType.Element && reader.Name == CubeNodeName)
            {
                if (IsNodeWithNewDate(reader))
                {
                    cubeDateTimeAttributeValue = GetDateFromCube(reader);
                }
                else
                {
                    return CreateNewCurrencyRateEntity(reader, cubeDateTimeAttributeValue);
                }
            }

            return null;
        }

        private static bool IsNodeWithNewDate(XmlTextReader reader)
        {
            return reader.GetAttribute(CubeTimeAttribute) != null;
        }


        private CurrencyRate CreateNewCurrencyRateEntity(XmlTextReader reader, DateTime cubeDateTimeAttributeValue)
        {
            decimal rate;
            bool isRateParsed = Decimal.TryParse(reader.GetAttribute(RateAttributeName), NumberStyles.Any,
                CultureInfo.InvariantCulture, out rate);
            string rateType = reader.GetAttribute(TypeAttributeName);

            if (IsCubeElementWithRates(isRateParsed, rateType))
            {
                return new CurrencyRate(rate, rateType, cubeDateTimeAttributeValue);
            }
            else
            {
                // this is  top(empty) Cube element
                return null;
            }
        }


        private static bool IsCubeElementWithRates(bool isRateParsed, string rateType)
        {
            return isRateParsed &&
                   !string.IsNullOrEmpty(rateType);
        }


        private static DateTime GetDateFromCube(XmlTextReader reader)
        {
            DateTime cubeTimeAttributeValue;
            bool isDateParsed = DateTime.TryParse(reader.GetAttribute(CubeTimeAttribute), out cubeTimeAttributeValue);
            if (!isDateParsed)
                throw new ArgumentOutOfRangeException("isDateParsed",
                    "attribute time of Element Cube was not parsed correctly. XML is currepted");

            return cubeTimeAttributeValue;
        }

    }
}
