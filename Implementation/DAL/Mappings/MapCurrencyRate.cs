﻿using System.Data.Entity.ModelConfiguration;
using DomainModel.Entities;

namespace Implementation.DAL.Mappings
{
    public class MapCurrencyRate : EntityTypeConfiguration<CurrencyRate>
    {
        public MapCurrencyRate()
        {
            ToTable("CurrencyRates");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id");
            Property(x => x.Rate).HasColumnName("Rate");
            Property(x => x.RateType).HasColumnName("RateType").HasColumnType("VARCHAR").HasMaxLength(5);
            Property(x => x.RateDate).HasColumnName("RateDate");
        }
    }

}
