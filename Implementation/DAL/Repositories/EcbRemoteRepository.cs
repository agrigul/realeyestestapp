﻿using System.Collections.Generic;
using System.Security.Policy;
using System.Xml;
using DomainModel.Entities;
using DomainModel.Repositories;
using DomainModel.Services;
using DomainModel.Utilities;

namespace Implementation.DAL.Repositories
{
    public class EcbRemoteRepository : IEcbRemoteRepository
    {
        private IParseXmlCurrencyRateService ParsingService { get; set; }

        private Url Url { get; set; }
        public EcbRemoteRepository(IParseXmlCurrencyRateService parsingServece, Url url)
        {
            Assert.IsNotNull(parsingServece, "parsingServece", typeof(IParseXmlCurrencyRateService));
            
            Url = url;
            ParsingService = parsingServece;
        }

        public IList<CurrencyRate> GetAllRates()
        {
            using (var reader = new XmlTextReader(Url.Value))
            {
                return ParsingService.GetListOfRates(reader);    
            }
            
        }

    }
}
