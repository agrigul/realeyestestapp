﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DomainModel.Repositories;
using DomainModel.Utilities;

namespace Implementation.DAL.Repositories
{
    /// <summary>
    /// Generic repository
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        protected readonly DbContext dbContext;
        public DbSet<TEntity> EntitySet { get; private set; }

        public GenericRepository(DbContext context)
        {
            Assert.IsNotNull(context, context.ToString(), typeof(DbContext));
            dbContext = context;
            EntitySet = context.Set<TEntity>();
        }

        /// <summary>
        /// Returns all entities
        /// </summary>
        /// <returns></returns>
        public IList<TEntity> GetAll()
        {
            return EntitySet.ToList();
        }

        /// <summary>
        /// Returns entity by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual TEntity GetById(object id)
        {
            return EntitySet.Find(id);
        }


        /// <summary>
        /// Inserts entity
        /// </summary>
        /// <param name="entity"></param>
        public virtual void Insert(TEntity entity)
        {
            Assert.IsNotNull(entity, "entity", typeof(TEntity));

            EntitySet.Add(entity);
            dbContext.Entry(entity).State = EntityState.Added;

        }

        /// <summary>
        /// Delete entity by id
        /// </summary>
        /// <param name="id"></param>
        public virtual void Delete(object id)
        {
            Assert.IsNotNull(id, "id", typeof(object));
            TEntity entityToDelete = EntitySet.Find(id);
            Delete(entityToDelete);
        }

        /// <summary>
        /// Deletes set of entities
        /// </summary>
        /// <param name="entities"></param>
        public virtual void DeleteAll(List<TEntity> entities)
        {
            Assert.IsNotNullOrEmpty((IList<object>)entities, "");

            foreach (var entity in entities)
            {
                this.Delete(entity);
            }
        }

        /// <summary>
        /// Detaches and removes entity
        /// </summary>
        /// <param name="entityToDelete"></param>
        public virtual void Delete(TEntity entityToDelete)
        {
            if (dbContext.Entry(entityToDelete).State == EntityState.Detached)
            {
                EntitySet.Attach(entityToDelete);
            }
            EntitySet.Remove(entityToDelete);
        }

        /// <summary>
        /// Updates entity
        /// </summary>
        /// <param name="entityToUpdate"></param>
        public virtual void Update(TEntity entityToUpdate)
        {
            EntitySet.Attach(entityToUpdate);
            dbContext.Entry(entityToUpdate).State = EntityState.Modified;
        }

        /// <summary>
        /// Find entity by expression
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public virtual IQueryable<TEntity> FindWhere(Expression<Func<TEntity, bool>> predicate)
        {
            return EntitySet.Where(predicate);
        }

        public virtual IQueryable<TEntity> GetQuery()
        {
            return EntitySet.AsQueryable();
        }


        /// <summary>
        /// Saves state to database
        /// </summary>
        public virtual void Save()
        {
            dbContext.SaveChanges();
        }

        /// <summary>
        ///  flag which shows that object was disposed
        /// </summary>
        private bool isDisposed;

        /// <summary>
        /// Disposes using isDisposed flag
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!isDisposed && dbContext != null)
            {
                if (disposing)
                {
                    dbContext.Dispose();
                }
            }
            isDisposed = true;
        }

        /// <summary>
        /// Dispose method implementation
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}