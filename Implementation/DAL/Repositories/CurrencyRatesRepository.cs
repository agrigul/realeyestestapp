﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DomainModel.Entities;
using DomainModel.Repositories;
using DomainModel.Utilities;

namespace Implementation.DAL.Repositories
{
    public class CurrencyRatesRepository : GenericRepository<CurrencyRate>, ICurrencyRatesRepository
    {
        public CurrencyRatesRepository(DbContext context)
            : base(context)
        {
        }

        public IList<CurrencyRate> GetRatesHistory(string currencyType)
        {
            Assert.IsNotNull(currencyType, "currencyType", currencyType.GetType());
            
            return EntitySet.Where(x => x.RateType == currencyType)
                                    .OrderBy(x => x.RateDate)
                                    .ToList();

        }
        public void SaveOnlyNewRates(IList<CurrencyRate> ratesList)
        {
            //1. select last date in DB

            CurrencyRate rateWithLastDate = (from entity in EntitySet
                                             orderby entity.RateDate descending
                                             select entity).FirstOrDefault();



            IList<CurrencyRate> filteredRates = new List<CurrencyRate>();

            if (rateWithLastDate != null)
            {
                filteredRates = (from rate in ratesList
                                 where rate.RateDate > rateWithLastDate.RateDate
                                 select rate).ToList();
            }
            else
                filteredRates = ratesList;

            //3. save to database
            InsertAll(filteredRates);
            Save();
        }

        public IList<CurrencyRate> GetLatestRates()
        {

            DateTime latestDate = (from entity in EntitySet
                                   orderby entity.RateDate descending
                                   select entity.RateDate).FirstOrDefault();

            return (from entity in EntitySet
                    where entity.RateDate == latestDate
                    select entity).ToList();

        }


        private void InsertAll(IList<CurrencyRate> filteredRates)
        {
            Assert.IsNotNull(filteredRates, "filteredRates", filteredRates.GetType());

            EntitySet.AddRange(filteredRates);
        }
    }
}
