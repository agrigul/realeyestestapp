﻿using System.Collections.Generic;
using DomainModel.Entities;

namespace DomainModel.Repositories
{
    public interface IEcbRemoteRepository
    {
        IList<CurrencyRate> GetAllRates();
    }
}
