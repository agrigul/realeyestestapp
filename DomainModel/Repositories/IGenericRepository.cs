﻿using System;
using System.Collections.Generic;

namespace DomainModel.Repositories
{
    /// <summary>
    /// Generic repository
    /// </summary>
    public interface IGenericRepository<TEntity> : IDisposable where TEntity : class
    {
        /// <summary>
        /// Get entity by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        TEntity GetById(object id);

        /// <summary>
        /// Get all entities
        /// </summary>
        /// <returns></returns>
        IList<TEntity> GetAll();
        
        /// <summary>
        /// Adds new record
        /// </summary>
        /// <param name="entity"></param>
        void Insert(TEntity entity);

        /// <summary>
        /// Deletes by id
        /// </summary>
        /// <param name="id"></param>
        void Delete(object id);

        /// <summary>
        /// Deletes list of entities
        /// </summary>
        /// <param name="entities"></param>
        void DeleteAll(List<TEntity> entities);

        /// <summary>
        /// Deletes entity
        /// </summary>
        /// <param name="entityToDelete"></param>
        void Delete(TEntity entityToDelete);

        /// <summary>
        /// updates entity
        /// </summary>
        /// <param name="entityToUpdate"></param>
        void Update(TEntity entityToUpdate);

        /// <summary>
        /// Save the state to database
        /// </summary>
        void Save();
    }
}
