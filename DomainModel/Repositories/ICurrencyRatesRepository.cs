﻿using System;
using System.Collections.Generic;
using DomainModel.Entities;

namespace DomainModel.Repositories
{
    /// <summary>
    /// Special repository for currency rates
    /// </summary>
    public interface ICurrencyRatesRepository : IGenericRepository<CurrencyRate>
    {
        void SaveOnlyNewRates(IList<CurrencyRate> ratesList);

        IList<CurrencyRate> GetLatestRates();

        IList<CurrencyRate> GetRatesHistory(string currencyType);
    }
}
