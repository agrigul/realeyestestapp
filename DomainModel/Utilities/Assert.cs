﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DomainModel.Utilities
{
    /// <summary>
    /// Checks the 
    /// </summary>
    public static class Assert
    {
        private const string ArgumentCantBeNullMsg = "Argument can't be null";
        private const string ArgumentListCantBeNullOrEmptyMsg = "Argument list can't be null or empty";
        
        /// <summary>
        /// Checks value is not null
        /// </summary>
        /// <param name="value"></param>
        /// <param name="valueName"></param>
        /// <param name="valueType"></param>
        /// <returns>true or exception</returns>
        public static bool IsNotNull(object value, string valueName = "", string valueType = "")
        {
            if (value == null)
            {
                if (string.IsNullOrEmpty(valueName))
                {
                    throw new ArgumentNullException(ArgumentCantBeNullMsg);
                }
                else
                {
                    throw new ArgumentNullException(valueName, ArgumentCantBeNullMsg);
                }
            }

            return true;
        }


        /// <summary>
        /// Checks value is not null
        /// </summary>
        /// <param name="value"></param>
        /// <param name="valueName"></param>
        /// <param name="valueType"></param>
        /// <returns>true or exception</returns>
        public static bool IsNotNull(object value, string valueName = "", Type valueType = null)
        {
            if(valueType != null)
                return IsNotNull(value, valueName, valueType.ToString());
            else 
                return IsNotNull(value, valueName, string.Empty);
        }



        /// <summary>
        /// Checks list is not null or empty
        /// </summary>
        /// <param name="list"></param>
        /// <param name="listName"></param>
        /// <returns>true or exception</returns>
        public static bool IsNotNullOrEmpty(IList<object> list, string listName = "")
        {
            if (string.IsNullOrEmpty(listName))
            {
                listName = "list";
            }

            IsNotNull(list, listName, "");

            if (list.Any() == false)
            {
                throw new ArgumentOutOfRangeException(listName, ArgumentListCantBeNullOrEmptyMsg);
            }

            return true;
        }


    }
}
