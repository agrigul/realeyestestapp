﻿using System.Collections.Generic;
using System.Security.Policy;
using System.Xml;
using DomainModel.Entities;

namespace DomainModel.Services
{
    public interface IParseXmlCurrencyRateService
    {
        IList<CurrencyRate> GetListOfRates(XmlTextReader xmlTextReader);
        
    }
}
