﻿using System;

namespace DomainModel.Entities
{
    public class CurrencyRate : BaseEntity
    {
        protected CurrencyRate() { /*for entity framework automapping only*/}

        public CurrencyRate(decimal rate, string rateType, DateTime rateDate)
        {
            if (rate < 0)
                throw new ArgumentOutOfRangeException("rate", "rate can't be a negative value");

            if (string.IsNullOrEmpty(rateType))
                throw new ArgumentNullException("rateType", "currency RateType can't be empty of whitespace");

            Rate = rate;
            RateType = rateType;
            RateDate = rateDate;
        }

        public decimal Rate { get; protected set; }

        public string RateType { get; protected set; }

        public DateTime RateDate { get; protected set; }
    }
}
