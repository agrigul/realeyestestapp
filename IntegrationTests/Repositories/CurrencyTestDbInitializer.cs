using System.Data.Entity;
using Implementation.DAL;

namespace IntegrationTests.Repositories
{
    public class CurrencyTestDbInitializer : DropCreateDatabaseIfModelChanges<CurrencyDbContext>
    {
        protected override void Seed(CurrencyDbContext context)
        {
        }
    }
}