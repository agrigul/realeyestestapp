﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DomainModel.Entities;
using DomainModel.Repositories;
using Implementation.DAL;
using Implementation.DAL.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace IntegrationTests.Repositories
{
    [TestClass]
    public class CrudOperationsCurrencyRateRepository
    {

        private const decimal TestRate = 10;
        private static readonly DateTime TestDate = DateTime.Now;
        private readonly string sqlQueryDelete = string.Format("DELETE FROM [dbo].[CurrencyRates]");
        private readonly string sqlQueryCreateNew = string.Format("INSERT INTO [dbo].[CurrencyRates] ([Rate], [RateType], [RateDate]) Values({0}, '{1}', '{2}')",
                                                                  TestRate, "USD", TestDate);

        [ClassInitializeAttribute]
        public static void TestClassInit(TestContext context)
        {
            var appDataDir = AppDomain.CurrentDomain.BaseDirectory;
            AppDomain.CurrentDomain.SetData("DataDirectory", appDataDir);

            Database.SetInitializer(new CurrencyTestDbInitializer());
            using (var db = new CurrencyDbContext())
            {
                db.Database.Initialize(true);
            }

        }

        [TestInitialize]
        public void RemoveData()
        {
            using (var dbContext = new CurrencyDbContext())
            {
                dbContext.Database.ExecuteSqlCommand(sqlQueryDelete);
                dbContext.SaveChanges();
            }
        }


        [TestMethod]
        [Description("Read from CurrencyRates")]
        public void ReadCurrencYRateRepository_NewEntity_ItemAdded()
        {
            using (var dbContext = new CurrencyDbContext())
            {
                dbContext.Database.ExecuteSqlCommand(sqlQueryCreateNew); // prepares data in table
                dbContext.SaveChanges();

                IGenericRepository<CurrencyRate> repository = new GenericRepository<CurrencyRate>(dbContext);
                CurrencyRate addedItem = repository.GetAll().SingleOrDefault();

                Assert.IsNotNull(addedItem);
                Assert.IsTrue(addedItem.Rate == TestRate);
                Assert.IsNotNull(addedItem.RateType == "USD");
            }
        }


        [TestMethod]
        [Description("Insert into CurrencyRates")]
        public void InsertCurrencyRatesRepository_NewEntity_ItemAdded()
        {
            using (var dbContext = new CurrencyDbContext())
            {
                IGenericRepository<CurrencyRate> repository = new GenericRepository<CurrencyRate>(dbContext);
                AddNewRateEntityToDB(repository);
                CurrencyRate addedItem = repository.GetAll().SingleOrDefault();
                Assert.IsNotNull(addedItem);
                Assert.IsTrue(addedItem.Rate == TestRate);
                Assert.IsTrue(addedItem.RateType == "USD");
            }
        }


        [TestMethod]
        [Description("Save only new items into CurrencyRates")]
        public void InsertOnlyNewCurrencyRatesRepository_ListOfEntities_ItemsAdded()
        {
            List<CurrencyRate> addedItemsList;
            using (var dbContext = new CurrencyDbContext())
            {
                ICurrencyRatesRepository repository = new CurrencyRatesRepository(dbContext);
                var existingItem1 = new CurrencyRate(1, "usd", DateTime.Now.AddDays(-1));
                var existingItem2 = new CurrencyRate(2, "usd", DateTime.Now.AddDays(-2));
                repository.Insert(existingItem1);
                repository.Insert(existingItem2);
                repository.Save();

                IList<CurrencyRate> currencyRatesList = new List<CurrencyRate>();
                currencyRatesList.Add(existingItem1); // duplicated item
                currencyRatesList.Add(new CurrencyRate(3, "rub", DateTime.Now));
                currencyRatesList.Add(new CurrencyRate(4, "uah", DateTime.Now.AddDays(1)));
                currencyRatesList.Add(new CurrencyRate(5, "jpy", DateTime.Now.AddDays(1)));

                repository.SaveOnlyNewRates(currencyRatesList);

                addedItemsList = repository.GetAll().OrderBy(x => x.RateDate).ToList();

            }

            Assert.IsNotNull(addedItemsList);
            Assert.IsTrue(addedItemsList.Count == 5);
        }


        private void AddNewRateEntityToDB(IGenericRepository<CurrencyRate> repository)
        {
            var rate = new CurrencyRate(TestRate, "USD", TestDate);
            repository.Insert(rate);
            repository.Save();
        }

    }

}
