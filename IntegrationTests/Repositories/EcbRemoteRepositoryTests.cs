﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using DomainModel.Entities;
using DomainModel.Repositories;
using DomainModel.Services;
using Implementation.DAL.Repositories;
using Implementation.DAL.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace IntegrationTests.Repositories
{
    [TestClass]
    public class EcbRemoteRepositoryTests
    {
        private readonly Url url = new Url("http://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml");

        [TestMethod]
        public void GetAllRates_Url_ListOfRtes()
        {
            IParseXmlCurrencyRateService ecbXmlParsingService = new ParseEcbXmlCurrencyRatesService();
            IEcbRemoteRepository repository = new EcbRemoteRepository(ecbXmlParsingService, url);
            IList<CurrencyRate> listOfRates = repository.GetAllRates();

            Assert.IsNotNull(listOfRates);
            Assert.IsTrue(listOfRates.Any());
        }
    }
}
